provider "azurerm" { 
       subscription_id = "03f6b116-4deb-4ea2-a7e3-b35aa30b9358" 
       client_id = "291182f6-6937-48d4-9020-b4be657698a7" 
       client_secret = "6fb6f6fc-8c7c-4095-961f-61473cd0a0b8" 
       tenant_id = "27b3f93e-58af-49ff-b14a-2696e7aa8968" 
}

resource "azurerm_resource_group" "myterraformgroup" {
    name     = "Formation"
    location = "eastus"
    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_virtual_network" "myterraformnetwork" {
    name                = "myVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_subnet" "myterraformsubnet" {
    name                 = "mySubnet"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
    address_prefix       = "10.0.2.0/24"
}

resource "azurerm_public_ip" "myterraformpublicip1" {
    name                         = "myPublicIP1"
    location                     = "eastus"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_public_ip" "myterraformpublicip2" {
    name                         = "myPublicIP2"
    location                     = "eastus"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "myNetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    
    tags {
        environment = "Terraform Demo"
    }

security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_interface" "myterraformnic1" {
    name                = "myNIC1"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip1.id}"
    }

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_network_interface" "myterraformnic2" {
    name                = "myNIC2"
    location            = "eastus"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip2.id}"
    }

    tags {
        environment = "Terraform Demo"
    }
}


resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.myterraformgroup.name}"
    }

    byte_length = 8
}

resource "azurerm_storage_account" "mystorageaccount" {
    name                = "diag${random_id.randomId.hex}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    location            = "eastus"
    account_replication_type = "LRS"
    account_tier = "Standard"

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_virtual_machine" "myterraformvm1" {
    name                  = "VMformation1"
    location              = "eastus"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic1.id}"]
    vm_size               = "Standard_DS1_v2"

storage_os_disk {
        name              = "myOsDisk1"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

os_profile {
        computer_name  = "Vmformation1"
        admin_username = "azureuser"
    }
os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/azureuser/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCuLNHlfTxj/ZQFcG2xK1I/pemZdyNEDMMyXFj69gGWpuOvAQNK0fycDx5Bex7/md4BW0vN9MhAbMP+qtS04SBfYB4CxakLJ51EA4AAfcOl6hH98uW6XtzsEtFCUH1F/cfKtOFBdYN0h8H4XpjWLpgB6bWuVJHk10sSf7wKf7alI7WKXoHTz/We5KySByoWKXnqdCWFW0OGt71k3Rpuw+8XdKpUaRMVmQBQfqao0dtLWPvxZEjJkNDmAGADHWPSw1BG4T6od7mpMA7HF5fNFJdin9ihfclX/D440KnuSzJXJrLCPLv4YbCMLhgqYREFfTVwHoFOlTickrAQ33tBckAmbnwYXf4wYY3iTpBBxk32+2Npud3HQsGR0kn/AssGMsxR3ex2PCJc9Nx2w6rv1mPacJBBxCLUsTIsl6445EjKUg2B+MZ9d5rmOl/BsaRHewDVVWcjc8MJFd5C40ru1MgByCGBOhpty8RdGNVJ+rSUVrpu/vEfUckOereO+V3MApGqXwgctZ0MZe3kNfamE+jxDyQ0JdlTT1HFQliePX/C44evEkWcI6M6CCO+rfxos54ubWjSF5x7bsI4kRS+xtFmGXu+iQm6oAtt+A75UdvEOZXj/y1iCnNHxhiyRXFa+aKNh6YmuGcC3fEEP/NZyL7Yj5Rw3LQpUjJGUjnNAIRJyQ== stage@CentOs.formation"
        }
    }
boot_diagnostics {
        enabled     = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags {
        environment = "Terraform Demo"
    }
}

resource "azurerm_virtual_machine" "myterraformvm2" {
    name                  = "VMformation2"
    location              = "eastus"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic2.id}"]
    vm_size               = "Standard_DS1_v2"

storage_os_disk {
        name              = "myOsDisk2"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

os_profile {
        computer_name  = "Vmformation2"
        admin_username = "azureuser"
        admin_password = "test"
    }
os_profile_linux_config {
        disable_password_authentication = false
    }

boot_diagnostics {
        enabled     = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags {
        environment = "Terraform Demo"
    }
}

